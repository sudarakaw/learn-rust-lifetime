struct MyIterator<'a, T> {
    slice: &'a [T],
}

impl<'a, T> Iterator for MyIterator<'a, T> {
    type Item = &'a T;

    fn next(&mut self) -> Option<Self::Item> {
        // Get the first element and rest ([x:xs])
        let (elem, rest) = self.slice.split_first()?;

        // Set the rest of the elements as `self.slice`
        self.slice = rest;

        // Return the first element
        Some(elem)
    }
}

// NOTE: `'iter` here is analogus to `'a` above. Hear we just adding meaning to it.
struct MyMutableIterator<'iter, T> {
    slice: &'iter mut [T],
}

impl<'iter, T> Iterator for MyMutableIterator<'iter, T> {
    type Item = &'iter mut T;

    fn next<'next>(&'next mut self) -> Option<Self::Item> {
        // Re-borrow slice
        // Step 1: borrow `self.slice` as `&mut &mut [T]` in the lifetime of
        // `'next`.
        let slice = &mut self.slice;
        // Step 2: push `self.slice` (now in `'next`) lifetime to slice again as
        // `&mut [T].
        let slice = std::mem::replace(slice, &mut []);

        // Get the first element and rest ([x:xs])
        let (elem, rest) = slice.split_first_mut()?;

        // Set the rest of the elements as `self.slice`
        // NOTE: this also restores the re-borrowed `self.slice`
        self.slice = rest;

        // Return the first element
        Some(elem)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn immutable_iterator() {
        let collection = vec![1, 2, 3, 4];
        let wrapper = MyIterator {
            slice: &collection[..],
        };

        for (idx, elem) in wrapper.enumerate() {
            assert_eq!(*elem, collection[idx]);
        }
    }

    #[test]
    fn mutable_iterator() {
        let mut collection = vec![2, 3, 4, 5];
        let wrapper = MyMutableIterator {
            slice: &mut collection[..],
        };

        for (_idx, elem) in wrapper.enumerate() {
            *elem = *elem + 1;
        }

        assert_eq!(collection.get(0), Some(&3));
    }
}
